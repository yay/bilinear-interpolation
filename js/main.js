// Inspired by https://en.wikipedia.org/wiki/Bilinear_interpolation.

var createTest = (function (width, height) {
    return function (test, w, h) {
        w = w || width;
        h = h || height;

        var canvas = document.createElement('canvas');
        canvas.width = w;
        canvas.height = h;
        canvas.style.width = Math.round(w / devicePixelRatio);
        canvas.style.height = Math.round(h / devicePixelRatio);
        canvas.style.margin = '10px';
        document.body.appendChild(canvas);
        document.body.appendChild(document.createElement('hr'));

        var ctx = canvas.getContext('2d');
        ctx.fillStyle = 'rgba(255, 0, 0, 0.2)';
        ctx.strokeStyle = 'rgba(0, 0, 0, 0.5)';
        ctx.lineWidth = 2;
        ctx.textAlign = 'start';
        ctx.textBaseline = 'middle';
        ctx.font = '24px Verdana';

        test(ctx, w, h);
    };

})(400, 400);

function createLinearInterpolator(a, b) {
    return function (t) {
        return a * (1 - t) + b * t;
    };
}

function createColorInterpolator(c1, c2) {
    var rI = createLinearInterpolator(c1[0], c2[0]),
        gI = createLinearInterpolator(c1[1], c2[1]),
        bI = createLinearInterpolator(c1[2], c2[2]);

    return function (t) {
        return [Math.round(rI(t)), Math.round(gI(t)), Math.round(bI(t))];
    }
}

createTest(function (ctx, w, h) {
    var i, j, t,
        imageData = ctx.getImageData(0, 0, w, h),
        data = imageData.data,
        q1 = [0, 0, 255], // bottom-left
        q2 = [255, 0, 0], // top-left
        q3 = [0, 255, 0], // top-right
        q4 = [255, 0, 0]; // bottom-right

    // Interpolate horizontally.
    var q1_to_q4_interpolator = createColorInterpolator(q1, q4);
    var q2_to_q3_interpolator = createColorInterpolator(q2, q3);

    var bottom_colors = [],
        top_colors = [];

    for (i = 0; i < w; i++) {
        t = i / (w - 1);
        bottom_colors[i] = q1_to_q4_interpolator(t);
        top_colors[i] = q2_to_q3_interpolator(t);
    }

    // Interpolate vertically.
    for (i = 0; i < w; i++) {
        var bottom_color = bottom_colors[i];
        var top_color = top_colors[i];
        var vertical_interpolator = createColorInterpolator(top_color, bottom_color);
        for (j = 0; j < h; j++) {
            var t = j / (h - 1);
            var color = vertical_interpolator(t);
            var index = (i + j * w) * 4;
            data[index] = color[0];
            data[index + 1] = color[1];
            data[index + 2] = color[2];
            data[index + 3] = 255; // alpha
        }
    }

    ctx.putImageData(imageData, 0, 0);
});
